package com.dwabotana;

public class Mutable {

    Integer hashCode;

    Mutable(int hashCode) {
        this.hashCode = hashCode;
    }

    @Override
    public int hashCode() {
        return this.hashCode;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Mutable that = (Mutable) o;
        return this.hashCode.equals(that.hashCode);
    }

    @Override
    public String toString() {
        return String.valueOf(hashCode);
    }
}