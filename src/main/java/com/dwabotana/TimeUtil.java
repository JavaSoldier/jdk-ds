package com.dwabotana;

import java.math.BigDecimal;

public class TimeUtil {

    public BigDecimal calculateTimeDiff(long start, long finish) {
        BigDecimal startTime = BigDecimal.valueOf(start);
        BigDecimal finishTime = BigDecimal.valueOf(finish);

        return (finishTime.subtract(startTime)).divide(BigDecimal.valueOf(1000L), BigDecimal.ROUND_HALF_EVEN);
    }
}
