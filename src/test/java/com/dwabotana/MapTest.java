package com.dwabotana;

import com.dwabotana.util.TestUtils;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class MapTest {

    private int size = 10;
    private Map<Mutable, String> map = new HashMap<>();
    private String val = "the same value";
    private String newVal = "new val";
    private TestUtils listUtil = new TestUtils(100000, 100000);


    @Test
    public void getElementByIndexHM() {
        HashMap<Integer, Integer> map = (HashMap<Integer, Integer>) listUtil.initializeMap(new HashMap<Integer, Integer>());
        BigDecimal res = new BigDecimal(0);
        for (int i = 1; i <= listUtil.getSize(); i++) {
            res.add(BigDecimal.valueOf(map.get(i)));
        }
        System.out.println(res);
    }

    @Test
    public void testNoExpandAndOverrideKey() {
        fillMutableElementMap(map);
        assertEquals("must contain appropriate value", val, map.get(new Mutable(size - 1)));

        map.put(new Mutable(size - 1), newVal);

        assertEquals("key must be overridden", newVal, map.get(new Mutable(size - 1)));
        assertTrue("no size expand", map.size() == size);
    }

    @Test
    public void testMutableKeys() {
        fillMutableElementMap(map);
        assertEquals("values are the same", val, map.get(new Mutable(1)));
        assertEquals("values are the same", val, map.get(new Mutable(2)));
        assertEquals("values are the same", val, map.get(new Mutable(10)));
        //change key hash codes
        map.keySet().forEach(huy -> huy.hashCode = 1);
        //looks like its ok
        assertEquals("values are the same", val, map.get(new Mutable(1)));
        //but it don't
        assertNull("no equal key within the bucket", map.get(new Mutable(2)));
        assertNull("no equal key within the bucket", map.get(new Mutable(10)));
        map.put(new Mutable(2), newVal);
        assertEquals("element added above is accessible", newVal, map.get(new Mutable(2)));
//        assertEquals();    map.get(new Mutable(1));
        // now we have a lot of not unique 1 keys
        assertNotNull(map.remove(new Mutable(1)));
        assertNotNull(map.remove(new Mutable(2)));
        assertNull("data structure is not delete 1 el, but there still a lot of mutable(1) keys", map.remove(new Mutable(1)));
        assertNull("data structure is not get element, but there still a lot of mutable(1) keys", map.get(new Mutable(1)));
        // now data structure is not working correct - at least we cannot delete/get elements...
    }

    @Test
    public void testTreeMap() {
        TreeMap<Integer, String> treeMap = new TreeMap<>(inititializeMap());
        assertEquals("natural ordered from first 2 last", Optional.of(1), treeMap.keySet().stream().findFirst());
    }

    private Map<Integer, String> inititializeMap() {
        return Stream.of(new Object[][]{
                {2, "data2"},
                {4, "data4"},
                {1, "data1"},
                {8, "data8"},
                {6, "data6"},
                {3, "data3"},
                {7, "data7"},
                {5, "data5"}
        }).collect(Collectors.toMap(data -> (Integer) data[0], data -> (String) data[1]));
    }

    private void fillMutableElementMap(Map<Mutable, String> map) {
        for (int i = 1; i <= size; i++) {
            map.put(new Mutable(i), val);
        }
    }
}
