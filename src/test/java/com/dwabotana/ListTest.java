package com.dwabotana;

import com.dwabotana.util.TestUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertTrue;

/*
 * test with no assert used 4 performance check
 * */
public class ListTest {

    private ArrayList<Integer> arrList1;
    private ArrayList<Integer> arrList2;
    private LinkedList<Integer> linkList1;
    private LinkedList<Integer> linkList2;
    private int loopSize = 100000;
    private TestUtils util = new TestUtils(loopSize);

    /*
     * list sizes are the same
     * */
    @Before
    public void before() {
        arrList1 = (ArrayList<Integer>) util.fillIncreasingIntCollection(new ArrayList<>());
        arrList2 = (ArrayList<Integer>) util.fillDecreasingIntCollection(new ArrayList<>());
        linkList1 = (LinkedList<Integer>) util.fillIncreasingIntCollection(new LinkedList<>());
        linkList2 = (LinkedList<Integer>) util.fillDecreasingIntCollection(new LinkedList<>());
    }

    @Test
    public void addAllEndAL() {
        arrList1.addAll(arrList2);
    }

    @Test
    public void addAllEndLL() {
        linkList1.addAll(linkList2);
    }

    @Test
    public void addEndAL() {
        for (Integer val : arrList2) {
            arrList1.add(val);
        }
    }

    @Test
    public void addEndLL() {
        for (Integer val : linkList2) {
            linkList1.addLast(val);
        }
    }

    @Test
    @Ignore
    public void addLastLL() {
        //todo  WTF? how to merge linked lists? with no iteration!!??
        linkList1.addLast(linkList2.getFirst());
        assertTrue("", linkList1.size() == loopSize + 1);
    }


    @Test
    public void addAllMidAL() {
        arrList1.addAll(arrList1.size() / 2, arrList2);
    }

    @Test
    public void addAllMidLL() {
        linkList1.addAll(linkList1.size() / 2, linkList2);
        assertTrue("size must be initial arrlist1+arrlist2", linkList1.size() == (linkList2.size() * 2));
    }

    @Test
    public void addMidAL() {
        int mid = arrList1.size() / 2;
        for (Integer val : arrList2) {
            arrList1.add(mid++, val);
        }
        assertTrue("size must be initial arrlist1+arrlist2", arrList1.size() == (arrList2.size() * 2));

    }

    @Test
    public void addMidLL() {
        int mid = linkList1.size() / 2;
        for (Integer val : linkList2) {
            linkList1.add(mid++, val);
        }
        assertTrue("size must be initial linkList1+linkList2", linkList1.size() == (linkList2.size() * 2));

    }

    @Test
    public void addFirstAL() {
        for (Integer val : arrList2) {
            arrList1.add(0, val);
        }
    }

    @Test
    public void addFirstLL() {
        for (Integer val : linkList2) {
            linkList1.addFirst(val);
        }
    }

    @Test
    public void getElementByIndexAL() {
        List<Integer> list = (List<Integer>) util.fillIncreasingIntCollection(new ArrayList<>());
        BigDecimal res = new BigDecimal(0);
        for (Integer val : list) {
            res.add(BigDecimal.valueOf(val));
        }
    }

    @Test
    public void insertOrderAndDuplicates() {
        Character[] abcd = {'a', 'b', 'c', 'd'};
        Character[] efg = {'e', 'f', 'g', 'd'};
        Character[] merge = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'd'};
        List<Character> characters = new ArrayList<>();
        Collections.addAll(characters, abcd);
        assertTrue("order must be maintained", characters.equals(Arrays.asList(abcd)));
        Collections.addAll(characters, efg);
        assertTrue("order must be maintained", characters.equals(Arrays.asList(merge)));
    }
}
