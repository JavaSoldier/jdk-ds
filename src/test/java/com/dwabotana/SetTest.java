package com.dwabotana;

import com.dwabotana.util.TestUtils;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SetTest {
    List<Integer> reverseDigits = Arrays.asList(5, 4, 3, 2, 1);
    List<Integer> naturalOrderingDigits = Arrays.asList(1, 2, 3, 4, 5);
    private TestUtils listUtil = new TestUtils(100000, 100000);


    @Test
    public void testAddHS() {
        HashSet<Integer> set = (HashSet<Integer>) listUtil.fillRandomOrderIntCollection(new HashSet<>());
        set.addAll(listUtil.fillIncreasingIntCollection(new HashSet<>()));
    }

    @Test
    public void testAddLHS() {
        LinkedHashSet<Integer> set = (LinkedHashSet<Integer>) listUtil.fillRandomOrderIntCollection(new LinkedHashSet<>());
        set.addAll(listUtil.fillIncreasingIntCollection(new HashSet<>()));
    }

    @Test
    public void testAddTS() {
        TreeSet<Integer> set = (TreeSet<Integer>) listUtil.fillRandomOrderIntCollection(new TreeSet<>());
        set.addAll(listUtil.fillIncreasingIntCollection(new HashSet<>()));
    }

    @Test
    public void testOrder() {
        Set<Integer> set = new HashSet<>(reverseDigits);
        assertFalse("order is not guaranteed", isValuesEquals(set, reverseDigits));
    }

    @Test
    public void testOrder2() {
        Set<Integer> set = new LinkedHashSet<>(reverseDigits);
        assertTrue("insertion order is guaranteed", isValuesEquals(set, reverseDigits));
    }

    @Test
    public void testOrder3() {
        Set<Integer> set = new TreeSet<>(reverseDigits);
        assertTrue("insertion order is not guaranteed, but natural ordering does", isValuesEquals(set, naturalOrderingDigits));
    }

    @Test
    public void testNotComparable() {
        Set<NcNumber> set = new TreeSet<>();
        set.addAll(Arrays.asList(new NcNumber(3), new NcNumber(2), new NcNumber(1), new NcNumber(1)));
        assertTrue(set.size() == 4);//tree set use compareTo instead equals
    }

    private boolean isValuesEquals(Set set, List list) {
        Iterator listIterator = list.iterator();
        Iterator setIterator = set.iterator();
        while (listIterator.hasNext() && setIterator.hasNext()) {
            if (!(setIterator.next().equals(listIterator.next()))) {
                return false;
            }
        }
        return true;
    }

    private class NcNumber implements Comparable {
        private Integer val;

        public NcNumber(Integer val) {
            this.val = val;
        }

        @Override
        public int compareTo(Object o) {
            return 1;
        }

        @Override
        public int hashCode() {
            return val.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return val.equals((Integer) obj);//fast and dirty
        }

        @Override
        public String toString() {
            return String.valueOf(val);
        }
    }


}
